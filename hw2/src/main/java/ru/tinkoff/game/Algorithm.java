package ru.tinkoff.game;


import javafx.util.Pair;
public class Algorithm {

    int[][] desk;
    int deskSize;

    private final int ENEMY_OF = -1;
    private final int DRAW_POINTS = 1;
    private final int DEEP = 1;

    private final int FINDING_POSITION = -1;

    private final int NOT_BE_IN = 0;
    private final int maxPoints;
    Algorithm (int[][] desk, int deskSize) {
        this.desk = desk;
        this.deskSize = deskSize;
        maxPoints = deskSize * deskSize;
    }

    public Pair<Integer, Integer> searchNextStep(int whichMove) {

        if(isEndGameFor(ENEMY_OF * whichMove))
            return new Pair<>(FINDING_POSITION, ENEMY_OF * whichMove * (maxPoints + 1) + DEEP);

        Pair<Integer , Integer> bestMove = searchBestPosition(whichMove);

        if(isDraw(bestMove.getValue(), ENEMY_OF * whichMove))
            return new Pair<>(FINDING_POSITION, DRAW_POINTS);

        return new Pair<>(bestMove.getKey(),bestMove.getValue() + DEEP);

    }

    private boolean isDraw(int score, int enemyMove) {
        return score == ((maxPoints + DEEP + 1) * enemyMove);
    }

    public Pair<Integer, Integer> searchBestPosition(int whichMove) {
        Pair<Integer, Integer> bestMove = new Pair<>(0, (maxPoints + DEEP + 1) * (ENEMY_OF * whichMove));

        int position = 0;

        for (int[] raw : desk) {
            for(int col : raw){
                int cordX = position % deskSize;
                int cordY = position / deskSize;
                if(col == NOT_BE_IN) {
                    desk[cordY][cordX] = whichMove;
                    Pair<Integer, Integer> nextBest = searchNextStep(ENEMY_OF * whichMove); //ищем лучший ход для этой клетки

                    if(isNextMoveBetter(whichMove, nextBest, bestMove)) {
                        bestMove = new Pair<>(position, nextBest.getValue());
                    }

                    desk[cordY][cordX] = NOT_BE_IN;
                }
                position++;
            }
        }
        return bestMove;
    }



    private boolean isNextMoveBetter(int whichMove, Pair<Integer, Integer> nextBest, Pair<Integer, Integer> bestMove) {
        return whichMove * nextBest.getValue() > whichMove * bestMove.getValue();
    }

    public boolean isEndGameFor(int whichMove) {

        boolean rawsAndColsChecked = checkRawsCols(whichMove);

        boolean diagChecked = checkDiag(whichMove);

        return rawsAndColsChecked || diagChecked;
    }

    private boolean checkRawsCols(int whichMove) {
        boolean raws = false;
        boolean cols = false;

        for(int yCord = 0;yCord < deskSize;yCord++) {
            boolean currentRaw = true;
            boolean currentCol = true;
            for(int xCord = 0;xCord < deskSize;xCord++) {
                if(desk[yCord][xCord] != whichMove)
                    currentRaw = false;
                if(desk[xCord][yCord] != whichMove)
                    currentCol = false;
            }
            raws = raws || currentRaw;
            cols = cols || currentCol;
        }

        return raws || cols;
    }

    private boolean checkDiag(int whichMove) {
        boolean diagRight = true;
        boolean diagLeft = true;

        for(int diagEl = 0;diagEl < deskSize;diagEl++) {
            if(desk[diagEl][diagEl] != whichMove)
                diagRight = false;
            int revDiagEl = deskSize - diagEl - 1;
            if(desk[revDiagEl][diagEl] != whichMove)
                diagLeft = false;
        }

        return diagRight || diagLeft;
    }

}

