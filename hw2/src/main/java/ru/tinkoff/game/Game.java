package ru.tinkoff.game;

import javafx.util.Pair;

import java.util.Scanner;

public class Game {
    private static Scanner input = new Scanner(System.in);

    private static final int CORRECT_DESK_SIZE = 3;
    private static final int COMPUTER = 1;
    private static final int NOT_BE_IN = 0;
    private static final int USER = -1;
    public static void start() {
        Algorithm game = prepareComputer();
        runGame(game);
    }

    private static Algorithm prepareComputer() {
        int deskSize = getDeskSize();

        int[][] desk = new int[deskSize][deskSize];

        Algorithm game = new Algorithm(desk, deskSize);

        return game;
    }

    private static int getDeskSize() {

        System.out.print("Enter desk size: ");
        int deskSize = input.nextInt();

        while(deskSize < CORRECT_DESK_SIZE) {
            System.out.println("Incorrect input, desk size > 2, please try again ");
            deskSize = input.nextInt();
        }

        return deskSize;
    }


    private static void runGame(Algorithm game){
        while (true) {

            drawDesk(game.desk);

            userMove(game);
            if (game.isEndGameFor(USER)) {
                System.out.println("Win User");
                break;
            }

            if(isDraw(game)) {
                System.out.println("It's draw!");
                break;
            }

            computerMove(game);
            if (game.isEndGameFor(COMPUTER)) {
                System.out.println("Win Computer");
                break;
            }



        }
    }

    private static boolean isDraw(Algorithm game) {
        boolean isEndGame = true;
        var desk = game.desk;
        for(var raw : desk) {
            for(var col : raw) {
                if(col == NOT_BE_IN)
                    isEndGame = false;
            }
        }
        return isEndGame;
    }


    private static void drawDesk(int[][] desk) {

        for(int[] raw : desk){
            for(int col : raw){
                System.out.print(drawPlayer(col) + " ");
            }
            System.out.println();
        }
    }

    private static String drawPlayer(int whichMove) {
        if(whichMove == NOT_BE_IN)
            return ".";
        if (whichMove == COMPUTER)
            return "X";
        return "O";
    }

    private static void userMove(Algorithm game) {
        System.out.println("Next Move?");

        Pair<Integer , Integer> userMove = getUserMove(game);

        int raw = userMove.getKey();
        int col = userMove.getValue();

        game.desk[raw][col] = USER;
    }

    private static Pair<Integer, Integer> getUserMove(Algorithm game) {
        int desksSize = game.deskSize;

        int raw = input.nextInt();
        int col = input.nextInt();
        boolean notCurrentRaw = desksSize - raw < 0;
        boolean notCurrentCol = desksSize - col < 0;

        while(notCurrentRaw || notCurrentCol) {
            System.out.println("Incorrent input, please try again");
            raw = input.nextInt();
            col = input.nextInt();
            notCurrentRaw = desksSize - raw < 0;
            notCurrentCol = desksSize - col < 0;
        }

        return new Pair<>(raw , col);

    }

    private static void computerMove(Algorithm game) {
        Pair<Integer, Integer> compMove = game.searchNextStep(COMPUTER);
        int raw = compMove.getKey() / (game.deskSize);
        int col = compMove.getKey() % (game.deskSize);
        game.desk[raw][col] = COMPUTER;
    }
}
