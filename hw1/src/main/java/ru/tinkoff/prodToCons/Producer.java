package ru.tinkoff.prodToCons;

public class Producer implements Runnable {
    private Store store;


    public Producer(Store store) {
        this.store = store;
    }

    public void run() {
        while(store.isNotFull()) {
            store.put(new Product());
        }
    }
}
