package ru.tinkoff.prodToCons;

public class Consumer implements Runnable {
    private String name;
    private Store store;

    public Consumer(String name,Store store) {
        this.name = name;
        this.store = store;
    }

    String getName() {
        return name;
    }

    public void run() {
        while(store.isNotEmpty()) {
            store.get(this);
        }

    }

}
