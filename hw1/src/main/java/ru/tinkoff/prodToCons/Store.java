package ru.tinkoff.prodToCons;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Store {

    private final int MAX_PRODUCTS = 100;

    private BlockingQueue<Product> store;

    public Store() {
        store = new LinkedBlockingQueue<>();
    }

    Product get(Consumer consumer) {
        try {
            Product product = store.take();
            System.out.println(consumer.getName() + "get product");
            return product;
        }
        catch (InterruptedException e) {}

        return null;
    }

    void put(Product product) {
        System.out.println("Producer put product");
        store.add(product);
    }

    boolean isNotFull() {
        return store.size() < MAX_PRODUCTS;
    }

    boolean isNotEmpty() {
        return store.size() > 0;
    }

}
