package ru.tinkoff;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.stream.Collectors;
import ru.tinkoff.prodToCons.Producer;
import ru.tinkoff.prodToCons.Consumer;
import ru.tinkoff.prodToCons.Store;


import static java.util.stream.Stream.of;

public class hw1 {
    public static void main(String[] args) {
        prodToConsBQ();
    }
    //Task1
    public static List<Integer> square56(List<Integer> list) {
        return  list.stream()
                .map(x -> x * x + 10)
                .filter(x -> (x % 10 != 5) && (x % 10 != 6))
                .toList();
    }
    //Task2
    public static Map<Integer,Integer> toMapus(List<Integer> list) {
        Map<Integer,Integer> ans = new HashMap<>();

        list.stream()
                .forEach(key -> ans.merge(key, 1,(oldValue , nextValue) -> oldValue + nextValue));

        return ans.entrySet()
                .stream()
                .filter(elem -> elem.getValue() != 1)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
    //Task3

    public static void prodToConsBQ() {
        Store store = new Store();
        Producer producer = new Producer(store);
        new Thread(producer).start();
        List<Thread> consumers = new ArrayList<>();
        for (int i = 0;i < 5;i++) {
            consumers.add(new Thread(new Consumer("Consumer_" + i ,store)));
        }

        for (var consumer : consumers) {
            consumer.start();
        }

    }

}
